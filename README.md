# Project 5: Brevet time calculator with Ajax and MongoDB

Author: Lucas Hyatt

This project implements a Brevet time calculator using AJAX. The algorith can be
found inside of the acp_times.py module. The server side is implemented within the flask_brevets.py and the client side is found within templates/calc.html.

The program can be run using the run shell script in the brevet directory. Brevet control times rely on the following table for calculation:

![Table](./table.png)

On top of this functionality, the project includes database storage (MongoDB) for saving brevet control values. After values have been calculated in the table, hit the submit button to save them. If you would like to see the contents of your database, click display to be redirected to a new database.

Test cases for this include checking whether or not values have actually been entered before submit is clicked, and whether the database includes values before display is clicked.

For questions, email llh@uoregon.edu